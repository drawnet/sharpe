package drawnet.lib.solvers;

import drawnet.lib.ddl.ElementInstance;
import drawnet.lib.ddl.propertyvalues.FixedArrayPropertyValue;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class SolverFilterDTMC extends SolverFilter {
	private final String DESCR = "Generate Sharpe file";
	private final static List<String> FLIST = new ArrayList<>
	(Arrays.asList("n", "ns", "intra", "inter", "bnet", "ec",
			"ff", "tStep", "T", "engine", "clusters", 
			"evidence", "cell", "filtering", "loglik"));

	private String error;
	private String warning;
	int numError;

	private Enumeration<ElementInstance> enDTMC;
	private ElementInstance current;
	private String outputFile;
	private String systemName;
	private boolean absorbingStateAnalysis;
	private boolean rewardAnalysis;
	private ArrayList<ElementInstance> nodes =
	new ArrayList<ElementInstance>();
	private ArrayList<ElementInstance> arcs =
	new ArrayList<ElementInstance>();
	private ArrayList<ElementInstance> absorbingState =
	new ArrayList<ElementInstance>();

	/**
	 * Constructor.
	 */
	public SolverFilterDTMC() {
		super();
		setDescription(DESCR);
	}

	/**
	 * Constructor.
	 *
	 * @param listener
	 */
	public SolverFilterDTMC(SolverListener listener) {
		super(listener);
		setDescription(DESCR);
	}

	public boolean execute() {
		FileOutputStream log;
		PrintStream write = null;

		try {
			log = new FileOutputStream("DTMC/DTMC_error.txt");
			write = new PrintStream(log);
		} catch (IOException e) {
			System.out.println("Error: " + e);
			error = error + "\n" + e;
		}
		System.out.println("\nExecuting " + this.getClass().getName());

		reset();
		check();

		write.println(error);
		if (!warning.isEmpty())
			write.println(warning);
		setCommandLine("CMDLINE1", "./DTMC/DTMC_solver.bat");
		numError = (error.split("\n").length) - 1;
		if (numError == 0) {
			creationFile();
			write.println("File saved: " + 
			System.getProperty("user.dir") + "/" + 
			outputFile + " whith 0 errors");
			System.out.println("File LOG saved: " + 
			System.getProperty("user.dir") + 
			"/DTMC/DTMC_error.txt");
			System.out.println("File saved: " + 
			System.getProperty("user.dir") + "/" + 
			outputFile + " whith 0 errors");
		} else {
			write.println("File NOT saved! errors: " + numError);
			write.println("File LOG saved in:" + 
			System.getProperty("user.dir") + "/DTMC/DTMC_error.txt");
		}

		if (sysCall() != 0) {
			System.out.println("syscall false");
			return false;
		}
		return true;
	}

	private void reset() {
		error = "";
		warning = "";
		outputFile = "";
		systemName = "";
		absorbingStateAnalysis = false;
		rewardAnalysis = false;
		absorbingState.clear();
		nodes.clear();
		arcs.clear();
	}

	private void check() {
		if (model_.getMainElement() == null) {
			System.out.println("model null");
			error = error + "\nERROR: Modello vuoto!";
		}
		// number of elements in main
		enDTMC = model_.getMainElement().subElementsEnum();
		if (enDTMC == null) {
			System.out.println("Modello vuoto!");
			error = error + "\nERROR: Modello vuoto!";
		}

		try {
			while (enDTMC.hasMoreElements()) {
				current = enDTMC.nextElement();
				switch (current.getElementType().getId().trim()) {
				case "State":
					nodes.add(current);
					break;
				case "Arc":
					arcs.add(current);
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			error = error + "\n" + e.getMessage();
		}

		if (!nodes.isEmpty() && arcs.isEmpty()) {
			System.out.println("Non ci sono archi");
			error += "\nERROR: non ci sono archi";
		}
		boolean error_format_size = false;
		int selected_query = 0;
		for (ElementInstance el : nodes) {
			int size = 0;
			float reward = 0;
			if (el.getPropertyValue("Query").toString().equals("true"))
				selected_query++;
			try {
				size = Integer.parseInt(el.getPropertyValue("size").toString().trim());
			} catch (NumberFormatException e) {
				System.out.println("La size di " +
				el.getId() + " deve essere di tipo intero");
				error += "\nERROR: La size di " +
				el.getId() + " deve essere di tipo intero";
				error_format_size = true;
			}

			try { // controllo sul tipo del reward
				reward = Float.parseFloat(el.getPropertyValue("Reward").toString().trim());
				if (reward > 0)
					rewardAnalysis = true; // analisi reward da effettuare
			} catch (NumberFormatException e) {
				System.out.println("La reward di " +
				el.getId() + " deve essere di tipo float");
				error += "\nERROR: La reward di " +
				el.getId() + " deve essere di tipo float";
				error_format_size = true;
			}

			if (reward < 0) {
				System.out.println("Il reward  di " +
				el.getId() + " è negativo");
				error += "\nERROR: Il reward di " +
				el.getId() + " è negativo";
			}

		}

		if (selected_query == 0) {
			System.out.println(
					"Non è stato selezionato alcuno stato per"+
					" effettuare un query, selezionare almeno uno stato");
			error += "\nNon è stato selezionato alcuno stato per "+
			"effettuare un query, selezionare almeno uno stato";
		}
		if (error_format_size) {
			return;
		}

		// Flag analisi stati assorbenti
		absorbingStateAnalysis = model_.getMainElement()
		.getPropertyValue("Absorbing_state_analysis").toString().trim()
		.equals("true");

		// Controllo sulla probabilità degli archi e sull'auto ricorsione
		boolean error_format_probability = false;
		for (ElementInstance arc : arcs) {
			float prob = 0;
			try {
				prob = Float.parseFloat(arc.getPropertyValue("Probability").toString().trim());
				if (prob < 0 || prob > 1) {
					System.out.println("L'arco " + arc.getId()
							+ " possiede una probabilità non valida (maggiore di 1 o minore di 0)");
					error += "\nERROR: L'arco " + arc.getId()
							+ " possiede una probabilità non valida (maggiore di 1 o minore di 0)";
				}
				if (arc.getPropertyValue("from").toString().equals(arc.getPropertyValue("to").toString())) {

					System.out.println("L'arco ricorsivo " + arc.getId()
							+ " può essere eliminato in quanto"+
							"verrà contato come 1-(sommatoria degli archi uscenti)");
					warning += "\nWARNING: L'arco ricorsivo " + arc.getId()
							+ " può essere eliminato in quanto "+
							"verà contanto come 1-(sommatoria degli archi uscenti)";
				}
			} catch (NumberFormatException e) {
				System.out.println("La probabilità dell'arco " + arc.getId() 
				+ " deve essere di tipo float");
				error += "\nERROR: La probabilità di " + arc.getId() 
				+ " deve essere di tipo float";
				error_format_probability = true;
			}
		}
		if (error_format_probability) {
			return;
		}

		// Controllo sugli archi (sommatoria archi uscenti compresi [0,1])
		for (ElementInstance node : nodes) {
			float sum = 0;
			int nArc = 0;
			for (ElementInstance arc : arcs) {
				String from = arc.getPropertyValue("from").toString();
				if (from.equals(node.getId().toString())) {
					sum += Float.parseFloat(arc.getPropertyValue("Probability").toString().trim());
					if (!from.equals(arc.getPropertyValue("to").toString()))
						nArc++;
				}
			}
			if (sum > 1 || sum < 0) {
				System.out.println("La somma  della probabilità degli archi uscenti da " + node.getId()
						+ " deve essere compresa tra [0,1] ");
				error += "\nERROR: La somma  della probabilità degli archi uscenti da " + node.getId()
						+ " non è compresa tra [0,1]";
			}

			if (nArc == 0) // è uno stato assorbente ( non ha archi uscenti, oppure è un unico arco
							// ricorsovo)
				absorbingState.add(node);

			sum = 0;
		}

		outputFile = model_.getMainElement().getPropertyValue("Output_File").toString().trim();
		if (outputFile.isEmpty()) {
			System.out.println("Il nome del file di output non puo' essere nullo");
			error = error + "\nERROR: Il nome del file di output non puo' essere nullo";
		}

		systemName = model_.getMainElement().getPropertyValue("System_Name").toString().trim();
		if (systemName.isEmpty()) {
			System.out.println("Il System name non puo' essere nullo");
			error = error + "\nERROR: Il System name non puo' essere nullo";
		}
	}

	private void creationFile() {

		try {
			File f = new File(System.getProperty("user.dir") + "/" + outputFile);
			FileWriter fw = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(fw);

			if (!f.exists()) {
				f.createNewFile();
			}

			// INIZIO FILE
			bw.write("markov " + systemName);

			// PROBABILITÀ ARCHI
			for (ElementInstance arc : arcs) {
				if (!arc.getPropertyValue("from").toString().equals(arc.getPropertyValue("to").toString())) {
					bw.newLine();
					BigDecimal prob = new BigDecimal(arc.getPropertyValue("Probability").toString().trim());
					bw.write(arc.getPropertyValue("from") + " " + arc.getPropertyValue("to") + " "
							+ prob.toPlainString()); // genero il grafo
				}
			}

			// REWARD
			boolean first = true;
			for (ElementInstance node : nodes) {
				BigDecimal reward = new BigDecimal(node.getPropertyValue("Reward")
				.toString().trim());
				if (reward.floatValue() > 0) {
					if (first) {
						bw.newLine();
						bw.write("reward");
						first = false;
					}
					bw.newLine();
					bw.write(node.getId() + " " + reward.toPlainString());
				}
			}

			bw.newLine();
			bw.write("end");
			bw.newLine();
			bw.newLine();

			
			if (absorbingState.size() > 0) { 
			// se ci sono stati assorbenti devo inserire le probabilità iniziali
				bw.newLine();
				bw.write("* Initial probabilities");
				bw.newLine();
				BigDecimal prob = new BigDecimal(1);
				prob = prob.divide(new BigDecimal(nodes.size()), 10, RoundingMode.FLOOR);
				for (ElementInstance node : nodes) {
					bw.newLine();
					bw.write(node.getId() + " " + prob.toPlainString());
				}
				bw.newLine();
				bw.write("end");
				bw.newLine();
			}

			// QUERY
			for (ElementInstance node : nodes) {

				if (node.getPropertyValue("Query").toString().equals("true")) { 
				// se è una query genera la stringa per
											// analizzare il nodo
					bw.newLine();
					bw.write("expr prob(" + systemName + "," + node.getId().toString() + ")");
				}
			}

			// ANALISI STATI ASSORBENTI
			if (absorbingState.size() > 0) {
				bw.newLine();
				bw.newLine();
				bw.write("echo Absorbing state Analysis");
				bw.newLine();
				bw.newLine();
				bw.write("expr mean(" + systemName + ")");
			}
			if (absorbingStateAnalysis && absorbingState.size() > 0) { 
			// è presente uno stato assorbente ed il flag è// vero
				for (ElementInstance node : absorbingState) {
					bw.newLine();
					bw.write("expr mean(" + systemName +
					"," + node.getId().toString().trim() + ")");
				}

			}

			// ANALISI REWARD PER LA STEADY STATE ANALYSIS
			if (rewardAnalysis && absorbingState.size() == 0) {
				bw.newLine();
				bw.newLine();
				bw.write("echo Expected Reward S.S. Analysis");
				bw.newLine();
				bw.write("expr exrss(" + systemName + ")");
				bw.newLine();
			}

			bw.newLine();
			bw.write("end"); // FINE DEL FILE
			bw.newLine();

			bw.close();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("File not saved IOExcetion\n");
			error += "\n ERROR: file not saved IOException";
		}
	}
}
