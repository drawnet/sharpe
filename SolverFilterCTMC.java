package drawnet.lib.solvers;

import drawnet.lib.ddl.ElementInstance;
import drawnet.lib.ddl.propertyvalues.FixedArrayPropertyValue;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class SolverFilterCTMC extends SolverFilter {
	private final String DESCR = "Generate Sharpe file";
	private final static List<String> FLIST = new ArrayList<>(Arrays.asList("n", "ns", "intra", "inter", "bnet", "ec",
			"ff", "tStep", "T", "engine", "clusters", "evidence", "cell", "filtering", "loglik"));

	private String error;
	private String warning;
	int numError;

	private Enumeration<ElementInstance> enCTMC;
	private ElementInstance current;
	private String outputFile;
	private String systemName;
	private boolean steadyStateAnalysis;
	private boolean absorbingStateAnalysis;
	private boolean rewardAnalysis;
	private BigDecimal stepTime;
	private BigDecimal finalStepTime;
	private float probInitNodes;
	private ArrayList<ElementInstance> nodes = new ArrayList<ElementInstance>();
	private ArrayList<ElementInstance> arcs = new ArrayList<ElementInstance>();
	private ArrayList<ElementInstance> absorbingState = new ArrayList<ElementInstance>();

	/**
	 * Constructor.
	 */
	public SolverFilterCTMC() {
		super();
		setDescription(DESCR);
	}

	/**
	 * Constructor.
	 *
	 * @param listener
	 */
	public SolverFilterCTMC(SolverListener listener) {
		super(listener);
		setDescription(DESCR);
	}

	public boolean execute() {
		FileOutputStream log;
		PrintStream write = null;

		try {
			log = new FileOutputStream("CTMC/CTMC_error.txt");
			write = new PrintStream(log);
		} catch (IOException e) {
			System.out.println("Error: " + e);
			error = error + "\n" + e;
		}
		System.out.println("\nExecuting " + this.getClass().getName());
		reset();
		check();

		write.println(error);
		if (!warning.isEmpty())
			write.println(warning);
		setCommandLine("CMDLINE1", "./CTMC/CTMC_solver.bat");
		numError = (error.split("\n").length) - 1;
		if (numError == 0) {
			creationFile();
			write.println("File saved: " + System.getProperty("user.dir") + "/" + outputFile + " whith 0 errors");
			System.out.println("File LOG saved: " + System.getProperty("user.dir") + "/CTMC/CTMC_error.txt");
			System.out.println("File saved: " + System.getProperty("user.dir") + "/" + outputFile + " whith 0 errors");
		} else {
			write.println("File NOT saved! errors: " + numError);
			write.println("File LOG saved in:" + System.getProperty("user.dir") + "/CTMC/CTMC_error.txt");
		}

		if (sysCall() != 0) {
			System.out.println("syscall false");
			return false;
		}
		return true;

	}

	private void reset() {
		error = "";
		warning = "";
		outputFile = "";
		systemName = "";
		stepTime = null;
		finalStepTime = null;
		steadyStateAnalysis = false;
		absorbingStateAnalysis = false;
		nodes.clear();
		arcs.clear();
		absorbingState.clear();
	}

	private void check() {
		if (model_.getMainElement() == null) {
			System.out.println("model null");
			error = error + "\nERROR: Modello vuoto!";
		}
		// number of elements in main
		enCTMC = model_.getMainElement().subElementsEnum();
		if (enCTMC == null) {
			System.out.println("Modello vuoto!");
			error = error + "\nERROR: Modello vuoto!";
		}

		try {
			while (enCTMC.hasMoreElements()) {
				current = enCTMC.nextElement();
				if (FLIST.contains(current.getId())) {
					System.out.println("Il nome: '" + current.getId()
							+ "' non puo' essere utilizzato perche' appartenente alla lista dinomi riservati:('n', 'ns', 'intra', 'inter', 'bnet', 'ec', 'ff', 'tStep', 'T', 'engine','evidence','cell','filtering','loglik')");
					error = error + "\nERROR: Il nome: '" + current.getId()
							+ "' non puo' essere utilizzato perche' appartenente alla lista dinomi riservati:('n', 'ns', 'intra', 'inter', 'bnet', 'ec', 'ff', 'tStep', 'T', 'engine','evidence','cell','filtering','loglik')";
				}
				switch (current.getElementType().getId().trim()) {
				case "State":
					nodes.add(current);
					break;
				case "Arc":
					arcs.add(current);
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			error = error + "\n" + e.getMessage();
		}

		if (!nodes.isEmpty() && arcs.isEmpty()) {
			System.out.println("Non ci sono archi");
			error += "\nERROR: non ci sono archi";
		}

		steadyStateAnalysis = model_.getMainElement().getPropertyValue("Steady_state_analysis").toString().trim()
				.equals("true");
		absorbingStateAnalysis = model_.getMainElement().getPropertyValue("Absorbing_state_analysis").toString().trim()
				.equals("true");

		for (ElementInstance node : nodes) {
			int nArc = 0;
			for (ElementInstance arc : arcs) {
				String from = arc.getPropertyValue("from").toString();
				if (from.equals(node.getId().toString())) {
					nArc++;
				}
			}

			if (nArc == 0) // è uno stato assorbente ( non ha archi uscenti)
				absorbingState.add(node);

		}

		boolean error_format_size = false;
		probInitNodes = 0; // per controllare durante la transient che la somma delle probabilità iniziali
							// non sia maggiore di 1 (allora normalizzo) e che almeno una query sia stata
							// selezionata
		int selected_query = 0;
		for (ElementInstance el : nodes) {
			int size = 0;
			float reward = 0;
			float probInit = 0;
			if (el.getPropertyValue("Query").toString().equals("true"))
				selected_query++;
			try {
				size = Integer.parseInt(el.getPropertyValue("size").toString().trim());
			} catch (NumberFormatException e) {
				System.out.println("La size di " + el.getId() + " deve essere di tipo intero");
				error += "\nERROR: La size di " + el.getId() + " deve essere di tipo intero";
				error_format_size = true;
			}

			try { // controllo sul tipo del reward
				reward = Float.parseFloat(el.getPropertyValue("Reward").toString().trim());
				if (reward > 0)
					rewardAnalysis = true; // almeno un reward deve essere impostato per poter generare l'analisi dei
											// reward
			} catch (NumberFormatException e) {
				System.out.println("La reward di " + el.getId() + " deve essere di tipo float");
				error += "\nERROR: La reward di " + el.getId() + " deve essere di tipo float";
				error_format_size = true;
			}

			if (reward < 0) {
				System.out.println("Il reward  di " + el.getId() + " è negativo");
				error += "\nERROR: Il reward di " + el.getId() + " è negativo";
			}

			try { // controllo sul tipo della prob.iniziale
				probInit = Float.parseFloat(el.getPropertyValue("Init_probability").toString().trim());
			} catch (NumberFormatException e) {
				System.out.println("La probabilità iniziale di " + el.getId() + " deve essere di tipo float");
				error += "\nERROR: La probabilità iniziale  di " + el.getId() + " deve essere di tipo float";
				error_format_size = true;
			}

			if (probInit < 0 || probInit > 1 && !steadyStateAnalysis) {
				System.out.println("La probabilità iniziale di " + el.getId() + " deve essere compresa tra [0,1]");
				error += "\nERROR: La probabilità iniziale  di " + el.getId() + " deve essere compresa tra [0,1]";
			}

			if (!steadyStateAnalysis || (steadyStateAnalysis && absorbingState.size() > 0)) {
				probInitNodes += probInit;
			}
		}

		if (selected_query == 0) {
			System.out.println(
					"Non è stato selezionato alcuno stato per effettuare un query, selezionare almeno uno stato");
			error += "\nNon è stato selezionato alcuno stato per effettuare un query, selezionare almeno uno stato";
		}

		if (probInitNodes < 0 || probInitNodes > 1) {
			System.out.println("La somma delle probabilità iniziali deve essere compresa tra [0,1]");
			error += "\nERROR: La somma delle probabilità iniziali deve essere compresa tra [0,1]";
		} else if (probInitNodes > 0 && probInitNodes < 1) {
			System.out.println(
					"La somma delle probabilità iniziali di tutti i nodi è inferiore ad 1, i valori verranno normalizzati a 1");
			warning += "\nWARNING: La somma delle probabilità iniziali di tutti i nodi è inferiore ad 1, i valori verranno normalizzati a 1";
		}

		if (error_format_size) {
			return;
		}

		// Controllo sulla probabilità degli archi e sull'auto ricorsione
		boolean error_format_probability = false;
		for (ElementInstance arc : arcs) {
			float prob = 0;
			try {
				prob = Float.parseFloat(arc.getPropertyValue("Transition_rate").toString().trim());
				if (prob < 0) {
					System.out.println(
							"L'arco " + arc.getId() + " possiede una transition rate non valida (minore di 0)");
					error += "\nERROR: L'arco " + arc.getId()
							+ " possiede una transition rate non valida (minore di 0)";
				}
				if (arc.getPropertyValue("from").toString().equals(arc.getPropertyValue("to").toString())) {
					System.out.println("Il nodo " + arc.getPropertyValue("from").toString() + "ha un arco ricorsivo");
					error += "\nERROR: Il nodo " + arc.getPropertyValue("from").toString() + "ha un arco ricorsivo";
				}
			} catch (NumberFormatException e) {
				System.out.println("La transition rate dell'arco " + arc.getId() + " deve essere di tipo float");
				error += "\nERROR: La transition rate di " + arc.getId() + " deve essere di tipo float";
				error_format_probability = true;
			}
		}
		if (error_format_probability) {
			return;
		}

		outputFile = model_.getMainElement().getPropertyValue("Output_File").toString().trim();
		if (outputFile.isEmpty()) {
			System.out.println("Il nome del file di output non puo' essere nullo");
			error = error + "\nERROR: Il nome del file di output non puo' essere nullo";
		}

		systemName = model_.getMainElement().getPropertyValue("System_Name").toString().trim();
		if (systemName.isEmpty()) {
			System.out.println("Il System name non puo' essere nullo");
			error = error + "\nERROR: Il System name non puo' essere nullo";
		}

		try {

			stepTime = new BigDecimal(model_.getMainElement().getPropertyValue("Step_time").toString().trim());
		} catch (NumberFormatException e) {
			System.out.println("Lo step time  del modello deve essere di tipo float");
			error += "\nERROR: Lo step time  del modello deve essere di tipo float";
		}
		if (stepTime.floatValue() <= 0 && !steadyStateAnalysis) {
			System.out.println("Lo step time  del modello deve essere strettamente positivo");
			error += "\nERROR: Lo step time  del modello deve essere strettamente positivo";
		}

		try {

			finalStepTime = new BigDecimal(
					model_.getMainElement().getPropertyValue("Final_step_time").toString().trim());
		} catch (NumberFormatException e) {
			System.out.println("Il final step time  del modello deve essere di tipo float");
			error += "\nERROR: Il final step time  del modello deve essere di tipo float";
		}

		if (finalStepTime.floatValue() <= 0 && !steadyStateAnalysis) {
			System.out.println("Il final step time  del modello deve essere strettamente positivo");
			error += "\nERROR: il final step time  del modello deve essere strettamente positivo";
		}

	}

	private void creationFile() {
		try {
			File f = new File(System.getProperty("user.dir") + "/" + outputFile);
			FileWriter fw = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(fw);

			if (!f.exists()) {
				f.createNewFile();
			}

			// INIZIO FILE
			if (!steadyStateAnalysis)
				bw.write("markov " + systemName + " readprobs");
			else
				bw.write("markov " + systemName);

			// PROBABILITÀ DEGLI ARCHI
			for (ElementInstance arc : arcs) {
				if (!arc.getPropertyValue("from").toString().equals(arc.getPropertyValue("to").toString())) {
					bw.newLine();
					BigDecimal tr = new BigDecimal(arc.getPropertyValue("Transition_rate").toString().trim());
					bw.write(
							arc.getPropertyValue("from") + " " + arc.getPropertyValue("to") + " " + tr.toPlainString()); // genero
																															// il
																															// grafo
				}
			}

			// REWARD DEGLI STATI
			boolean first = true;
			for (ElementInstance node : nodes) {
				BigDecimal reward = new BigDecimal(node.getPropertyValue("Reward").toString().trim());
				if (reward.floatValue() > 0) {
					if (first) {
						bw.newLine();
						bw.write("reward");
						first = false;
					}
					bw.newLine();
					bw.write(node.getId() + " " + reward.toPlainString());
				}
			}

			bw.newLine();
			bw.write("end");
			bw.newLine();

			// ANALISI TRANSIENT - PROBABILITÀ INIZIALI
			if (!steadyStateAnalysis || (steadyStateAnalysis && absorbingState.size() > 0)) { // se è stato settato lo
																								// steady state analysis
																								// allora non bisognerà
																								// inserire le
																								// probabilità iniziali,
																								// a meno che non sia
																								// una catena assorbente
				BigDecimal sumProb = new BigDecimal(probInitNodes);
				bw.newLine();
				bw.write("* Initial probabilities");
				bw.newLine();
				for (ElementInstance node : nodes) {
					BigDecimal prob = new BigDecimal(node.getPropertyValue("Init_probability").toString().trim());
					if (prob.floatValue() > 0) {
						if (probInitNodes < 1)
							prob = prob.divide(sumProb, 10, RoundingMode.FLOOR); // probabilità normalizzata ad 1
						bw.newLine();
						bw.write(node.getId() + " " + prob.toPlainString());
					}
				}
				bw.newLine();
				bw.write("end");
				bw.newLine();
			}

			// ANALISI STATI ASSORBENTI
			if (absorbingState.size() > 0) {
				bw.newLine();
				bw.newLine();
				bw.write("echo Absorbing state Analysis");
				bw.newLine();
				bw.newLine();
				bw.write("expr mean(" + systemName + ")");
			}

			if (absorbingStateAnalysis && absorbingState.size() > 0) { // è presente uno stato assorbente ed il flag è
																		// vero
				for (ElementInstance node : absorbingState) {
					bw.newLine();
					bw.write("expr mean(" + systemName + "," + node.getId().toString().trim() + ")");
				}
			}

			// ANALISI TRANSIENT
			if (!steadyStateAnalysis) { // se è stato settato lo steady state analysis allora non bisognerà inserire la
										// funzione per lo step time
				bw.newLine();
				bw.newLine();
				bw.write("echo Transient Analysis");
				bw.newLine();
				bw.newLine();
				bw.write("loop t,0," + finalStepTime.toPlainString() + "," + stepTime.toPlainString());

				for (ElementInstance node : nodes) {
					if (node.getPropertyValue("Query").toString().equals("true")) { // se è una query genera la stringa
																					// per analizzare il nodo
						bw.newLine();
						bw.write("expr tvalue(t," + systemName + "," + node.getId().toString() + ")");
					}
				}

				// ANALISI SUI REWARD
				if (rewardAnalysis) {
					bw.newLine();
					// bw.newLine();
					// bw.write("echo Expected Reward Analysis");
					bw.newLine();
					bw.write("expr cexrt(t," + systemName + ")");
					bw.newLine();
					bw.write("expr exrt(t," + systemName + ")");
				}
				bw.newLine();
				bw.newLine();
				bw.write("end");
				bw.newLine();
				bw.newLine();
				bw.write("end");
				bw.newLine();
			} else {

				// STEADY STATE ANALYSIS
				bw.newLine();
				bw.newLine();
				bw.write("echo Steady State Analysis");
				bw.newLine();
				for (ElementInstance node : nodes) {
					if (node.getPropertyValue("Query").toString().equals("true")) { // se è una query genera la stringa
																					// per analizzare il nodo
						bw.newLine();
						bw.write("expr prob(" + systemName + "," + node.getId().toString() + ")");
					}
				}

				// ANALISI REWARD PER LA S.S
				if (rewardAnalysis && absorbingState.size() == 0) {
					bw.newLine();
					bw.newLine();
					bw.write("echo Expected Reward S.S. Analysis");
					bw.newLine();
					bw.write("expr exrss(" + systemName + ")");
					bw.newLine();
				}

				bw.newLine();
				bw.write("end"); // FINE FILE
				bw.newLine();
			}

			bw.close();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("File not saved IOExcetion\n");
			error += "\n ERROR: file not saved IOException";
		}
	}
}
